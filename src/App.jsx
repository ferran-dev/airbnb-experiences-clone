import Navbar from './components/Navbar'
import Hero from './components/Hero'
import Card from './components/Card'
import experiencesData from './data'
import './App.css'

function App() {
  const path = "../images/"
  const cards = experiencesData.map(exp => {
    return (
      <Card
        key={exp.id}
        img={path + exp.coverImg}
        rating={exp.stats.rating}
        reviewCount={exp.stats.reviewCount}
        country={exp.location}
        title={exp.title}
        price={exp.price}
      />
    )
  }
  )

  return (
    <>
      <Navbar />
      <Hero />
      <section className='cards-list'>
        {cards}
      </section>
    </>
  )
}

export default App
