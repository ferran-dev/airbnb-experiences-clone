import React from "react"
import star from "../assets/star.png"

export default function Card(props) {
  return (
    <div className="card">
      <img src={props.img} className="card--img" alt="Experience cover" />
      <div className="card--info">
        <img src={star} className="star-icon" alt="Star" />{props.rating} <span className="card--info-ratings"> ({props.reviewCount}) • {props.country}</span>
      </div>
      <p className="card--description">{props.title}</p>
      <p className="card--price"><span className="bold">From ${props.price}</span> / person</p>
    </div>
  )
}