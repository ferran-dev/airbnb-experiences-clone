import headerImg from "../assets/photo-grid.png"

export default function Hero() {
  return (
    <section className="header">
      <img src={headerImg} className="header--img" alt="Experiences photo grid" />
      <div className="header--description">
        <h1>Online Experiences</h1>
        <p>Join unique interactive activities led by one-of-a-kind hosts—all without leaving home.</p>
      </div>
    </section>
  )
}